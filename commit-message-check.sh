#!/bin/bash
# Define the pattern for commit message structure
pattern="^\[commit-summary\]: .*\n\[commit-details\]: .*$"
commit_msg_file=$1
commit_msg=$(cat $commit_msg_file)
echo $commit_msg
 if the commit message matches the pattern
if  [[ $commit_msg == $pattern ]]
 then
    echo "Commit Done Successfully "
    exit 1
else
    echo "Error: Commit message does not follow the required pattern."
    echo "Please provide a commit message with the following structure:"
    echo "[commit-summary]: Code refactoring"
    echo "[commit-details]: Replaced POJOs with Spring boot "
    exit 1
fi
